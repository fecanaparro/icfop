//
//  CfopListViewController.m
//  Icfop
//
//  Created by Felix Canaparro on 19/03/13.
//  Copyright (c) 2013 Felix Canaparro. All rights reserved.
//

#import "CfopListViewController.h"
#import "Cfop.h"
#import "CfopDetailTableViewController.h"
@interface CfopListViewController ()

@end

@implementation CfopListViewController
@synthesize cfopArray;
@synthesize filteredCfopArray;
@synthesize cfopSearchBar;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


/*- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
 {
 NSPredicate *resultPredicate = [NSPredicate
 predicateWithFormat:@"SELF.codCfop contains[cd] %@",
 searchText];
 
 filteredCfopArray = [cfopArray filteredArrayUsingPredicate:resultPredicate];
 }*/
#pragma mark Content Filtering


-(void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope {
    // Update the filtered array based on the search text and scope.
    // Remove all objects from the filtered search array
    //    [self.filteredCfopArray removeAllObjects];
    // Filter the array using NSPredicate
    
    if ([scope isEqualToString:@"Codigo"])
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.codCfop contains[cd] %@",searchText];
        filteredCfopArray = [NSMutableArray arrayWithArray:[cfopArray filteredArrayUsingPredicate:predicate]];
    }else
        if ([scope isEqualToString:@"Descricao"])
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.descCfop contains[cd] %@",searchText];
            filteredCfopArray = [NSMutableArray arrayWithArray:[cfopArray filteredArrayUsingPredicate:predicate]];
        }
}


#pragma mark - UISearchDisplayController Delegate Methods
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    // Tells the table data source to reload when text changes
    [self filterContentForSearchText:searchString scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    // Tells the table data source to reload when scope bar selection changes
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [cfopSearchBar setShowsScopeBar:false];
    [cfopSearchBar sizeToFit];
    
    cfopArray = [NSArray arrayWithObjects:
                 [Cfop initWithCodCfop:@"1118" descCfop:@"Compra de mercadoria para comercialização pelo adquirente originário, entregue pelo vendedor remetente ao destinatário, em venda à ordem" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1120" descCfop:@"Compra para industrialização, em venda à ordem, já recebida do vendedor remetente" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1121" descCfop:@"Compra para comercialização, em venda à ordem, já recebida do vendedor remetente" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1122" descCfop:@"Compra para industrialização em que a mercadoria foi remetida pelo fornecedor ao industrializador sem transitar pelo estabelecimento adquirente" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1124" descCfop:@"Industrialização efetuada por outra empresa" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1125" descCfop:@"Industrialização efetuada por outra empresa quando a mercadoria remetida para utilização no processo de industrialização não transitou pelo estabelecimento adquirente da mercadoria" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1126" descCfop:@"Compra para utilização na prestação de serviço" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1151" descCfop:@"Transferência para industrialização" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1152" descCfop:@"Transferência para comercialização" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1153" descCfop:@"Transferência de energia elétrica para distribuição" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1154" descCfop:@"Transferência para utilização na prestação de serviço" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1201" descCfop:@"Devolução de venda de produção do estabelecimento" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1202" descCfop:@"Devolução de venda de mercadoria adquirida ou recebida de terceiros" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1203" descCfop:@"Devolução de venda de produção do estabelecimento, destinada à Zona Franca de Manaus ou Áreas de Livre Comércio" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1204" descCfop:@"Devolução de venda de mercadoria adquirida ou recebida de terceiros, destinada à Zona Franca de Manaus ou Áreas de Livre Comércio" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1205" descCfop:@"Anulação de valor relativo à prestação de serviço de comunicação" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1206" descCfop:@"Anulação de valor relativo à prestação de serviço de transporte" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1207" descCfop:@"Anulação de valor relativo à venda de energia elétrica" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1208" descCfop:@"Devolução de produção do estabelecimento, remetida em transferência" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1209" descCfop:@"Devolução de mercadoria adquirida ou recebida de terceiros, remetida em transferência" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1251" descCfop:@"Compra de energia elétrica para distribuição ou comercialização" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1252" descCfop:@"Compra de energia elétrica por estabelecimento industrial" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1253" descCfop:@"Compra de energia elétrica por estabelecimento comercial" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1254" descCfop:@"Compra de energia elétrica por estabelecimento prestador de serviço de transporte" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1255" descCfop:@"Compra de energia elétrica por estabelecimento prestador de serviço de comunicação" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1256" descCfop:@"Compra de energia elétrica por estabelecimento de produtor rural" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1257" descCfop:@"Compra de energia elétrica para consumo por demanda contratada" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1301" descCfop:@"Aquisição de serviço de comunicação para execução de serviço da mesma natureza" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1302" descCfop:@"Aquisição de serviço de comunicação por estabelecimento industrial" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1303" descCfop:@"Aquisição de serviço de comunicação por estabelecimento comercial" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1304" descCfop:@"Aquisição de serviço de comunicação por estabelecimento de prestador de serviço de transporte" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1305" descCfop:@"Aquisição de serviço de comunicação por estabelecimento de geradora ou de distribuidora de energia elétrica" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1306" descCfop:@"Aquisição de serviço de comunicação por estabelecimento de produtor rural" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1351" descCfop:@"Aquisição de serviço de transporte para execução de serviço da mesma natureza" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1352" descCfop:@"Aquisição de serviço de transporte por estabelecimento industrial" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1353" descCfop:@"Aquisição de serviço de transporte por estabelecimento comercial" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1354" descCfop:@"Aquisição de serviço de transporte por estabelecimento de prestador de serviço de comunicação" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1355" descCfop:@"Aquisição de serviço de transporte por estabelecimento de geradora ou de distribuidora de energia elétrica" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1356" descCfop:@"Aquisição de serviço de transporte por estabelecimento de produtor rural" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1401" descCfop:@"Compra para industrialização em operação com mercadoria sujeita ao regime de substituição tributária" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1403" descCfop:@"Compra para comercialização em operação com mercadoria sujeita ao regime de substituição tributária" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1406" descCfop:@"Compra de bem para o ativo imobilizado cuja mercadoria está sujeita ao regime de substituição tributária" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1407" descCfop:@"Compra de mercadoria para uso ou consumo cuja mercadoria está sujeita ao regime de substituição tributária" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1408" descCfop:@"Transferência para industrialização em operação com mercadoria sujeita ao regime de substituição tributária" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1409" descCfop:@"Transferência para comercialização em operação com mercadoria sujeita ao regime de substituição tributária" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1410" descCfop:@"Devolução de venda de produção do estabelecimento em operação com produto sujeito ao regime de substituição tributária" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1411" descCfop:@"Devolução de venda de mercadoria adquirida ou recebida de terceiros em operação com mercadoria sujeita ao regime de substituição tributária" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1414" descCfop:@"Retorno de produção do estabelecimento, remetida para venda fora do estabelecimento em operação com produto sujeito ao regime de substituição tributária" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1415" descCfop:@"Retorno de mercadoria adquirida ou recebida de terceiros, remetida para venda fora do estabelecimento em operação com mercadoria sujeita ao regime de substituição tributária" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1451" descCfop:@"Retorno de animal do estabelecimento produtor" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1452" descCfop:@"Retorno de insumo não utilizado na produção" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1501" descCfop:@"Entrada de mercadoria recebida com fim específico de exportação" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1503" descCfop:@"Entrada decorrente de devolução de produto remetido com fim específico de exportação, de produção do estabelecimento" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1504" descCfop:@"Entrada decorrente de devolução de mercadoria remetida com fim específico de exportação, adquirida ou recebida de terceiros" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1551" descCfop:@"Compra de bem para o ativo imobilizado" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1552" descCfop:@"Transferência de bem do ativo imobilizado" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1553" descCfop:@"Devolução de venda de bem do ativo imobilizado" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1554" descCfop:@"Retorno de bem do ativo imobilizado remetido para uso fora do estabelecimento" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1555" descCfop:@"Entrada de bem do ativo imobilizado de terceiro, remetido para uso no estabelecimento" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1556" descCfop:@"Compra de material para uso ou consumo" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1557" descCfop:@"Transferência de material para uso ou consumo" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1601" descCfop:@"Recebimento, por transferência, de crédito de ICMS" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1602" descCfop:@"Recebimento, por transferência, de saldo credor de ICMS de outro estabelecimento da mesma empresa, para compensação de saldo devedor de ICMS" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1603" descCfop:@"Ressarcimento de ICMS retido por substituição tributária" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1604" descCfop:@"Lançamento do crédito relativo à compra de bem para o ativo imobilizado" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1605" descCfop:@"Recebimento, por transferência, de saldo devedor de ICMS de outro estabelecimento da mesma empresa" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1650" descCfop:@"ENTRADAS DE COMBUSTÍVEIS, DERIVADOS OU NÃO DE PETRÓLEO E LUBRIFICANTES" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1651" descCfop:@"Compra de combustível ou lubrificante para industrialização subseqüente" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1652" descCfop:@"Compra de combustível ou lubrificante para comercialização" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1653" descCfop:@"Compra de combustível ou lubrificante por consumidor ou usuário final" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1658" descCfop:@"Transferência de combustível e lubrificante para industrialização" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1659" descCfop:@"Transferência de combustível e lubrificante para comercialização" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1660" descCfop:@"Devolução de venda de combustível ou lubrificante destinado à industrialização" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1661" descCfop:@"Devolução de venda de combustível ou lubrificante destinado à comercialização" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1662" descCfop:@"Devolução de venda de combustível ou lubrificante destinado a consumidor ou usuário final" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1663" descCfop:@"Entrada de combustível ou lubrificante para armazenagem" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1664" descCfop:@"Retorno de combustível ou lubrificante remetido para armazenagem" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1901" descCfop:@"Entrada para industrialização por encomenda" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1902" descCfop:@"Retorno de mercadoria remetida para industrialização por encomenda" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1903" descCfop:@"Entrada de mercadoria remetida para industrialização e não aplicada no referido processo" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1904" descCfop:@"Retorno de remessa para venda fora do estabelecimento" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1905" descCfop:@"Entrada de mercadoria recebida para depósito em depósito fechado ou armazém geral" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1906" descCfop:@"Retorno de mercadoria remetida para depósito fechado ou armazém geral" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1907" descCfop:@"Retorno simbólico de mercadoria remetida para depósito fechado ou armazém geral" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1908" descCfop:@"Entrada de bem por conta de contrato de comodato" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1909" descCfop:@"Retorno de bem remetido por conta de contrato de comodato" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1910" descCfop:@"Entrada de bonificação, doação ou brinde" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1911" descCfop:@"Entrada de amostra grátis" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1912" descCfop:@"Entrada de mercadoria ou bem recebido para demonstração" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1913" descCfop:@"Retorno de mercadoria ou bem remetido para demonstração" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1914" descCfop:@"Retorno de mercadoria ou bem remetido para exposição ou feira" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1915" descCfop:@"Entrada de mercadoria ou bem recebido para conserto ou reparo" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1916" descCfop:@"Retorno de mercadoria ou bem remetido para conserto ou reparo" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1917" descCfop:@"Entrada de mercadoria recebida em consignação mercantil ou industrial" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1918" descCfop:@"Devolução de mercadoria remetida em consignação mercantil ou industrial" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1919" descCfop:@"Devolução simbólica de mercadoria vendida ou utilizada em processo industrial, remetida anteriormente em consignação mercantil ou industrial" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1920" descCfop:@"Entrada de vasilhame ou sacaria" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1921" descCfop:@"Retorno de vasilhame ou sacaria" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1922" descCfop:@"Lançamento efetuado a título de simples faturamento decorrente de compra para recebimento futuro" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1923" descCfop:@"Entrada de mercadoria recebida do vendedor remetente, em venda à ordem" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1924" descCfop:@"Entrada para industrialização por conta e ordem do adquirente da mercadoria, quando esta não transitar pelo estabelecimento do adquirente" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1925" descCfop:@"Retorno de mercadoria remetida para industrialização por conta e ordem do adquirente da mercadoria, quando esta não transitar pelo estabelecimento do adquirente" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1926" descCfop:@"Lançamento efetuado a título de reclassificação de mercadoria decorrente de formação de kit ou de sua desagregação" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1931" descCfop:@"Lançamento efetuado pelo tomador do serviço de transporte quando a responsabilidade de retenção do imposto for atribuída ao remetente ou alienante da mercadoria, pelo serviço de transporte realizado por transportador autônomo ou por transportador não inscrito na Unidade da Federação onde iniciado o serviço " tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1932" descCfop:@"Aquisição de serviço de transporte iniciado em Unidade da Federação diversa daquela onde inscrito o prestador" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1933" descCfop:@"Aquisição de serviço tributado pelo ISSQN" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"1949" descCfop:@"Outra entrada de mercadoria ou prestação de serviço não especificada" tipoCfop:@"Entrada de fornecedor do mesmo estado"],
                 [Cfop initWithCodCfop:@"2101" descCfop:@"Compra para industrialização" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2102" descCfop:@"Compra para comercialização" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2111" descCfop:@"Compra para industrialização de mercadoria recebida anteriormente em consignação industrial" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2113" descCfop:@"Compra para comercialização, de mercadoria recebida anteriormente em consignação mercantil" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2116" descCfop:@"Compra para industrialização originada de encomenda para recebimento futuro" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2117" descCfop:@"Compra para comercialização originada de encomenda para recebimento futuro" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2118" descCfop:@"Compra de mercadoria para comercialização pelo adquirente originário, entregue pelo vendedor remetente ao destinatário, em venda à ordem" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2120" descCfop:@"Compra para industrialização, em venda à ordem, já recebida do vendedor remetente" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2121" descCfop:@"Compra para comercialização, em venda à ordem, já recebida do vendedor remetente" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2122" descCfop:@"Compra para industrialização em que a mercadoria foi remetida pelo fornecedor ao industrializador sem transitar pelo estabelecimento adquirente" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2124" descCfop:@"Industrialização efetuada por outra empresa" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2125" descCfop:@"Industrialização efetuada por outra empresa quando a mercadoria remetida para utilização no processo de industrialização não transitou pelo estabelecimento adquirente da mercadoria" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2126" descCfop:@"Compra para utilização na prestação de serviço" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2151" descCfop:@"Transferência para industrialização" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2152" descCfop:@"Transferência para comercialização" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2153" descCfop:@"Transferência de energia elétrica para distribuição" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2154" descCfop:@"Transferência para utilização na prestação de serviço" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2201" descCfop:@"Devolução de venda de produção do estabelecimento" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2202" descCfop:@"Devolução de venda de mercadoria adquirida ou recebida de terceiros" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2203" descCfop:@"Devolução de venda de produção do estabelecimento, destinada à Zona Franca de Manaus ou Áreas de Livre Comércio" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2204" descCfop:@"Devolução de venda de mercadoria adquirida ou recebida de terceiros, destinada à Zona Franca de Manaus ou Áreas de Livre Comércio" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2205" descCfop:@"Anulação de valor relativo à prestação de serviço de comunicação" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2206" descCfop:@"Anulação de valor relativo à prestação de serviço de transporte" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2207" descCfop:@"Anulação de valor relativo à venda de energia elétrica" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2208" descCfop:@"Devolução de produção do estabelecimento, remetida em transferência" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2209" descCfop:@"Devolução de mercadoria adquirida ou recebida de terceiros, remetida em transferência" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2251" descCfop:@"Compra de energia elétrica para distribuição ou comercialização" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2252" descCfop:@"Compra de energia elétrica por estabelecimento industrial" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2253" descCfop:@"Compra de energia elétrica por estabelecimento comercial" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2254" descCfop:@"Compra de energia elétrica por estabelecimento prestador de serviço de transporte" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2255" descCfop:@"Compra de energia elétrica por estabelecimento prestador de serviço de comunicação" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2256" descCfop:@"Compra de energia elétrica por estabelecimento de produtor rural" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2257" descCfop:@"Compra de energia elétrica para consumo por demanda contratada" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2301" descCfop:@"Aquisição de serviço de comunicação para execução de serviço da mesma natureza" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2302" descCfop:@"Aquisição de serviço de comunicação por estabelecimento industrial" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2303" descCfop:@"Aquisição de serviço de comunicação por estabelecimento comercial" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2304" descCfop:@"Aquisição de serviço de comunicação por estabelecimento de prestador de serviço de transporte" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2305" descCfop:@"Aquisição de serviço de comunicação por estabelecimento de geradora ou de distribuidora de energia elétrica" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2306" descCfop:@"Aquisição de serviço de comunicação por estabelecimento de produtor rural" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2351" descCfop:@"Aquisição de serviço de transporte para execução de serviço da mesma natureza" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2352" descCfop:@"Aquisição de serviço de transporte por estabelecimento industrial" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2353" descCfop:@"Aquisição de serviço de transporte por estabelecimento comercial" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2354" descCfop:@"Aquisição de serviço de transporte por estabelecimento de prestador de serviço de comunicação" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2355" descCfop:@"Aquisição de serviço de transporte por estabelecimento de geradora ou de distribuidora de energia elétrica" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2356" descCfop:@"Aquisição de serviço de transporte por estabelecimento de produtor rural" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2401" descCfop:@"Compra para industrialização em operação com mercadoria sujeita ao regime de substituição tributária" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2403" descCfop:@"Compra para comercialização em operação com mercadoria sujeita ao regime de substituição tributária" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2406" descCfop:@"Compra de bem para o ativo imobilizado cuja mercadoria está sujeita ao regime de substituição tributária" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2407" descCfop:@"Compra de mercadoria para uso ou consumo cuja mercadoria está sujeita ao regime de substituição tributária" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2408" descCfop:@"Transferência para industrialização em operação com mercadoria sujeita ao regime de substituição tributária" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2409" descCfop:@"Transferência para comercialização em operação com mercadoria sujeita ao regime de substituição tributária" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2410" descCfop:@"Devolução de venda de produção do estabelecimento em operação com produto sujeito ao regime de substituição tributária" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2411" descCfop:@"Devolução de venda de mercadoria adquirida ou recebida de terceiros em operação com mercadoria sujeita ao regime de substituição tributária" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2414" descCfop:@"Retorno de produção do estabelecimento, remetida para venda fora do estabelecimento em operação com produto sujeito ao regime de substituição tributária" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2415" descCfop:@"Retorno de mercadoria adquirida ou recebida de terceiros, remetida para venda fora do estabelecimento em operação com mercadoria sujeita ao regime de substituição tributária" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2501" descCfop:@"Entrada de mercadoria recebida com fim específico de exportação" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2503" descCfop:@"Entrada decorrente de devolução de produto remetido com fim específico de exportação, de produção do estabelecimento" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2504" descCfop:@"Entrada decorrente de devolução de mercadoria remetida com fim específico de exportação, adquirida ou recebida de terceiros" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2551" descCfop:@"Compra de bem para o ativo imobilizado" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2552" descCfop:@"Transferência de bem do ativo imobilizado" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2553" descCfop:@"Devolução de venda de bem do ativo imobilizado" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2554" descCfop:@"Retorno de bem do ativo imobilizado remetido para uso fora do estabelecimento" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2555" descCfop:@"Entrada de bem do ativo imobilizado de terceiro, remetido para uso no estabelecimento" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2556" descCfop:@"Compra de material para uso ou consumo" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2557" descCfop:@"Transferência de material para uso ou consumo" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2603" descCfop:@"Ressarcimento de ICMS retido por substituição tributária" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2651" descCfop:@"Entradas de combustiveis, derivados ou nao de petroleo e lubrificantes" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2652" descCfop:@"Compra de combustível ou lubrificante para comercialização" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2653" descCfop:@"Compra de combustível ou lubrificante por consumidor ou usuário final" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2658" descCfop:@"Transferência de combustível e lubrificante para industrialização" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2659" descCfop:@"Transferência de combustível e lubrificante para comercialização" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2660" descCfop:@"Devolução de venda de combustível ou lubrificante destinado à industrialização" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2661" descCfop:@"Devolução de venda de combustível ou lubrificante destinado à comercialização" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2662" descCfop:@"Devolução de venda de combustível ou lubrificante destinado a consumidor ou usuário final" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2663" descCfop:@"Entrada de combustível ou lubrificante para armazenagem" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2664" descCfop:@"Retorno de combustível ou lubrificante remetido para armazenagem" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2901" descCfop:@"Entrada para industrialização por encomenda" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2902" descCfop:@"Retorno de mercadoria remetida para industrialização por encomenda" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2903" descCfop:@"Entrada de mercadoria remetida para industrialização e não aplicada no referido processo" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2904" descCfop:@"Retorno de remessa para venda fora do estabelecimento" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2905" descCfop:@"Entrada de mercadoria recebida para depósito em depósito fechado ou armazém geral" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2906" descCfop:@"Retorno de mercadoria remetida para depósito fechado ou armazém geral" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2907" descCfop:@"Retorno simbólico de mercadoria remetida para depósito fechado ou armazém geral" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2908" descCfop:@"Entrada de bem por conta de contrato de comodato" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2909" descCfop:@"Retorno de bem remetido por conta de contrato de comodato" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2910" descCfop:@"Entrada de bonificação, doação ou brinde" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2911" descCfop:@"Entrada de amostra grátis" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2912" descCfop:@"Entrada de mercadoria ou bem recebido para demonstração" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2913" descCfop:@"Retorno de mercadoria ou bem remetido para demonstração" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2914" descCfop:@"Retorno de mercadoria ou bem remetido para exposição ou feira" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2915" descCfop:@"Entrada de mercadoria ou bem recebido para conserto ou reparo" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2916" descCfop:@"Retorno de mercadoria ou bem remetido para conserto ou reparo" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2917" descCfop:@"Entrada de mercadoria recebida em consignação mercantil ou industrial" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2918" descCfop:@"Devolução de mercadoria remetida em consignação mercantil ou industrial" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2919" descCfop:@"Devolução simbólica de mercadoria vendida ou utilizada em processo industrial, remetida anteriormente em consignação mercantil ou industrial" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2920" descCfop:@"Entrada de vasilhame ou sacaria" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2921" descCfop:@"Retorno de vasilhame ou sacaria" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2922" descCfop:@"Lançamento efetuado a título de simples faturamento decorrente de compra para recebimento futuro" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2923" descCfop:@"Entrada de mercadoria recebida do vendedor remetente, em venda à ordem" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2924" descCfop:@"Entrada para industrialização por conta e ordem do adquirente da mercadoria, quando esta não transitar pelo estabelecimento do adquirente" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2925" descCfop:@"Retorno de mercadoria remetida para industrialização por conta e ordem do adquirente da mercadoria, quando esta não transitar pelo estabelecimento do adquirente" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2931" descCfop:@"Lançamento efetuado pelo tomador do serviço de transporte quando a responsabilidade de retenção do imposto for atribuída ao remetente ou alienante da mercadoria, pelo serviço de transporte realizado por transportador autônomo ou por transportador não inscrito na Unidade da Federação onde iniciado o serviço " tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2932" descCfop:@"Aquisição de serviço de transporte iniciado em Unidade da Federação diversa daquela onde inscrito o prestador" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2933" descCfop:@"Aquisição de serviço tributado pelo ISSQN" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"2949" descCfop:@"Outra entrada de mercadoria ou prestação de serviço não especificado" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"3101" descCfop:@"Compra para industrialização" tipoCfop:@"Entrada de fornecedor de outro estado"],
                 [Cfop initWithCodCfop:@"3102" descCfop:@"Compra para comercialização" tipoCfop:@"Entrada Exterior"],
                 [Cfop initWithCodCfop:@"3126" descCfop:@"Compra para utilização na prestação de serviço" tipoCfop:@"Entrada Exterior"],
                 [Cfop initWithCodCfop:@"3127" descCfop:@"Compra para industrialização sob o regime de “drawback”" tipoCfop:@"Entrada Exterior"],
                 [Cfop initWithCodCfop:@"3201" descCfop:@"Devolução de venda de produção do estabelecimento" tipoCfop:@"Entrada Exterior"],
                 [Cfop initWithCodCfop:@"3202" descCfop:@"Devolução de venda de mercadoria adquirida ou recebida de terceiros" tipoCfop:@"Entrada Exterior"],
                 [Cfop initWithCodCfop:@"3205" descCfop:@"Anulação de valor relativo à prestação de serviço de comunicação" tipoCfop:@"Entrada Exterior"],
                 [Cfop initWithCodCfop:@"3206" descCfop:@"Anulação de valor relativo à prestação de serviço de transporte" tipoCfop:@"Entrada Exterior"],
                 [Cfop initWithCodCfop:@"3207" descCfop:@"Anulação de valor relativo à venda de energia elétrica" tipoCfop:@"Entrada Exterior"],
                 [Cfop initWithCodCfop:@"3211" descCfop:@"Devolução de venda de produção do estabelecimento sob o regime de “drawback”" tipoCfop:@"Entrada Exterior"],
                 [Cfop initWithCodCfop:@"3251" descCfop:@"Compra de energia elétrica para distribuição ou comercialização" tipoCfop:@"Entrada Exterior"],
                 [Cfop initWithCodCfop:@"3301" descCfop:@"Aquisição de serviço de comunicação para execução de serviço da mesma natureza" tipoCfop:@"Entrada Exterior"],
                 [Cfop initWithCodCfop:@"3351" descCfop:@"Aquisição de serviço de transporte para execução de serviço da mesma natureza" tipoCfop:@"Entrada Exterior"],
                 [Cfop initWithCodCfop:@"3352" descCfop:@"Aquisição de serviço de transporte por estabelecimento industrial" tipoCfop:@"Entrada Exterior"],
                 [Cfop initWithCodCfop:@"3353" descCfop:@"Aquisição de serviço de transporte por estabelecimento comercial" tipoCfop:@"Entrada Exterior"],
                 [Cfop initWithCodCfop:@"3354" descCfop:@"Aquisição de serviço de transporte por estabelecimento de prestador de serviço de comunicação" tipoCfop:@"Entrada Exterior"],
                 [Cfop initWithCodCfop:@"3355" descCfop:@"Aquisição de serviço de transporte por estabelecimento de geradora ou de distribuidora de energia elétrica" tipoCfop:@"Entrada Exterior"],
                 [Cfop initWithCodCfop:@"3356" descCfop:@"Aquisição de serviço de transporte por estabelecimento de produtor rural" tipoCfop:@"Entrada Exterior"],
                 [Cfop initWithCodCfop:@"3503" descCfop:@"Devolução de mercadoria exportada que tenha sido recebida com fim específico de exportação" tipoCfop:@"Entrada Exterior"],
                 [Cfop initWithCodCfop:@"3551" descCfop:@"Compra de bem para o ativo imobilizado" tipoCfop:@"Entrada Exterior"],
                 [Cfop initWithCodCfop:@"3553" descCfop:@"Devolução de venda de bem do ativo imobilizado" tipoCfop:@"Entrada Exterior"],
                 [Cfop initWithCodCfop:@"3556" descCfop:@"Compra de material para uso ou consumo" tipoCfop:@"Entrada Exterior"],
                 [Cfop initWithCodCfop:@"3650" descCfop:@"ENTRADAS DE COMBUSTÍVEIS, DERIVADOS OU NÃO DE PETRÓLEO E LUBRIFICANTES" tipoCfop:@"Entrada Exterior"],
                 [Cfop initWithCodCfop:@"3651" descCfop:@"Compra de combustível ou lubrificante para industrialização subseqüente" tipoCfop:@"Entrada Exterior"],
                 [Cfop initWithCodCfop:@"3652" descCfop:@"Compra de combustível ou lubrificante para comercialização" tipoCfop:@"Entrada Exterior"],
                 [Cfop initWithCodCfop:@"3653" descCfop:@"Compra de combustível ou lubrificante por consumidor ou usuário final" tipoCfop:@"Entrada Exterior"],
                 [Cfop initWithCodCfop:@"3930" descCfop:@"Lançamento efetuado a título de entrada de bem sob amparo de regime especial aduaneiro de admissão temporária" tipoCfop:@"Entrada Exterior"],
                 [Cfop initWithCodCfop:@"3949" descCfop:@"Outra entrada de mercadoria ou prestação de serviço não especificado" tipoCfop:@"Entrada Exterior"],
                 [Cfop initWithCodCfop:@"5101" descCfop:@"Venda de produção do estabelecimento" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5102" descCfop:@"Venda de mercadoria adquirida ou recebida de terceiros" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5103" descCfop:@"Venda de produção do estabelecimento, efetuada fora do estabelecimento" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5104" descCfop:@"Venda de mercadoria adquirida ou recebida de terceiros, efetuada fora do estabelecimento" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5105" descCfop:@"Venda de produção do estabelecimento que não deva por ele transitar" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5106" descCfop:@"Venda de mercadoria adquirida ou recebida de terceiros, que não deva por ele transitar" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5109" descCfop:@"Venda de produção do estabelecimento, destinada à Zona Franca de Manaus ou Áreas de Livre Comércio" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5110" descCfop:@"Venda de mercadoria adquirida ou recebida de terceiros, destinada à Zona Franca de Manaus ou Áreas de Livre Comércio" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5111" descCfop:@"Venda de produção do estabelecimento remetida anteriormente em consignação industrial" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5112" descCfop:@"Venda de mercadoria adquirida ou recebida de terceiros remetida anteriormente em consignação industrial" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5113" descCfop:@"Venda de produção do estabelecimento remetida anteriormente em consignação mercantil" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5114" descCfop:@"Venda de mercadoria adquirida ou recebida de terceiros remetida anteriormente em consignação mercantil" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5115" descCfop:@"Venda de mercadoria adquirida ou recebida de terceiros, recebida anteriormente em consignação mercantil" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5116" descCfop:@"Venda de produção do estabelecimento originada de encomenda para entrega futura" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5117" descCfop:@"Venda de mercadoria adquirida ou recebida de terceiros, originada de encomenda para entrega futura" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5118" descCfop:@"Venda de produção do estabelecimento entregue ao destinatário por conta e ordem do adquirente originário, em venda à ordem" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5119" descCfop:@"Venda de mercadoria adquirida ou recebida de terceiros entregue ao destinatário por conta e ordem do adquirente originário, em venda à ordem" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5120" descCfop:@"Venda de mercadoria adquirida ou recebida de terceiros entregue ao destinatário pelo vendedor remetente, em venda à ordem" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5122" descCfop:@"Venda de produção do estabelecimento remetida para industrialização, por conta e ordem do adquirente, sem transitar pelo estabelecimento do adquirente" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5123" descCfop:@"Venda de mercadoria adquirida ou recebida de terceiros remetida para industrialização, por conta e ordem do adquirente, sem transitar pelo estabelecimento do adquirente" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5124" descCfop:@"Industrialização efetuada para outra empresa" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5125" descCfop:@"Industrialização efetuada para outra empresa quando a mercadoria recebida para utilização no processo de industrialização não transitar pelo estabelecimento adquirente da mercadoria" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5151" descCfop:@"Transferência de produção do estabelecimento" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5152" descCfop:@"Transferência de mercadoria adquirida ou recebida de terceiros" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5153" descCfop:@"Transferência de energia elétrica" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5155" descCfop:@"Transferência de produção do estabelecimento, que não deva por ele transitar" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5156" descCfop:@"Transferência de mercadoria adquirida ou recebida de terceiros, que não deva por ele transitar" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5201" descCfop:@"Devolução de compra para industrialização" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5202" descCfop:@"Devolução de compra para comercialização" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5205" descCfop:@"Anulação de valor relativo a aquisição de serviço de comunicação" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5206" descCfop:@"Anulação de valor relativo a aquisição de serviço de transporte" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5207" descCfop:@"Anulação de valor relativo à compra de energia elétrica" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5208" descCfop:@"Devolução de mercadoria recebida em transferência para industrialização" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5209" descCfop:@"Devolução de mercadoria recebida em transferência para comercialização" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5210" descCfop:@"Devolução de compra para utilização na prestação de serviço" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5251" descCfop:@"Venda de energia elétrica para distribuição ou comercialização" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5252" descCfop:@"Venda de energia elétrica para estabelecimento industrial" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5253" descCfop:@"Venda de energia elétrica para estabelecimento comercial" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5254" descCfop:@"Venda de energia elétrica para estabelecimento prestador de serviço de transporte" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5255" descCfop:@"Venda de energia elétrica para estabelecimento prestador de serviço de comunicação" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5256" descCfop:@"Venda de energia elétrica para estabelecimento de produtor rural" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5257" descCfop:@"Venda de energia elétrica para consumo por demanda contratada" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5258" descCfop:@"Venda de energia elétrica a não contribuinte" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5301" descCfop:@"Prestação de serviço de comunicação para execução de serviço da mesma natureza" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5302" descCfop:@"Prestação de serviço de comunicação a estabelecimento industrial" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5303" descCfop:@"Prestação de serviço de comunicação a estabelecimento comercial" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5304" descCfop:@"Prestação de serviço de comunicação a estabelecimento de prestador de serviço de transporte" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5305" descCfop:@"Prestação de serviço de comunicação a estabelecimento de geradora ou de distribuidora de energia elétrica" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5306" descCfop:@"Prestação de serviço de comunicação a estabelecimento de produtor rural" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5307" descCfop:@"Prestação de serviço de comunicação a não contribuinte" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5351" descCfop:@"Prestação de serviço de transporte para execução de serviço da mesma natureza" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5352" descCfop:@"Prestação de serviço de transporte a estabelecimento industrial" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5353" descCfop:@"Prestação de serviço de transporte a estabelecimento comercial" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5354" descCfop:@"Prestação de serviço de transporte a estabelecimento de prestador de serviço de comunicação" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5355" descCfop:@"Prestação de serviço de transporte a estabelecimento de geradora ou de distribuidora de energia elétrica" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5356" descCfop:@"Prestação de serviço de transporte a estabelecimento de produtor rural" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5357" descCfop:@"Prestação de serviço de transporte a não contribuinte" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5359" descCfop:@"Prestação de serviço de transporte a contribuinte ou a não contribuinte quando a mercadoria transportada está dispensada de emissão de nota fiscal" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5401" descCfop:@"Venda de produção do estabelecimento em operação com produto sujeito ao regime de substituição tributária, na condição de contribuinte substituto" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5402" descCfop:@"Venda de produção do estabelecimento de produto sujeito ao regime de substituição tributária, em operação entre contribuintes substitutos do mesmo produto" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5403" descCfop:@"Venda de mercadoria adquirida ou recebida de terceiros em operação com mercadoria sujeita ao regime de substituição tributária, na condição de contribuinte substituto" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5405" descCfop:@"Venda de mercadoria adquirida ou recebida de terceiros em operação com mercadoria sujeita ao regime de substituição tributária, na condição de contribuinte substituído" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5408" descCfop:@"Transferência de produção do estabelecimento em operação com produto sujeito ao regime de substituição tributária" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5409" descCfop:@"Transferência de mercadoria adquirida ou recebida de terceiros em operação com mercadoria sujeita ao regime de substituição tributária" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5410" descCfop:@"Devolução de compra para industrialização em operação com mercadoria sujeita ao regime de substituição tributária" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5411" descCfop:@"Devolução de compra para comercialização em operação com mercadoria sujeita ao regime de substituição tributária" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5412" descCfop:@"Devolução de bem do ativo imobilizado, em operação com mercadoria sujeita ao regime de substituição tributária" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5413" descCfop:@"Devolução de mercadoria destinada ao uso ou consumo, em operação com mercadoria sujeita ao regime de substituição tributária" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5414" descCfop:@"Remessa de produção do estabelecimento para venda fora do estabelecimento em operação com produto sujeito ao regime de substituição tributária" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5415" descCfop:@"Remessa de mercadoria adquirida ou recebida de terceiros para venda fora do estabelecimento, em operação com mercadoria sujeita ao regime de substituição tributária" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5451" descCfop:@"Remessa de animal e de insumo para estabelecimento produtor" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5501" descCfop:@"Remessa de produção do estabelecimento, com fim específico de exportação" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5502" descCfop:@"Remessa de mercadoria adquirida ou recebida de terceiros, com fim específico de exportação" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5503" descCfop:@"Devolução de mercadoria recebida com fim específico de exportação" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5551" descCfop:@"Venda de bem do ativo imobilizado" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5552" descCfop:@"Transferência de bem do ativo imobilizado" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5553" descCfop:@"Devolução de compra de bem para o ativo imobilizado" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5554" descCfop:@"Remessa de bem do ativo imobilizado para uso fora do estabelecimento" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5555" descCfop:@"Devolução de bem do ativo imobilizado de terceiro, recebido para uso no estabelecimento" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5556" descCfop:@"Devolução de compra de material de uso ou consumo" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5557" descCfop:@"Transferência de material de uso ou consumo" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5601" descCfop:@"Transferência de crédito de ICMS acumulado" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5602" descCfop:@"Transferência de saldo credor de ICMS para outro estabelecimento da mesma empresa, destinado à compensação de saldo devedor de ICMS" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5603" descCfop:@"Ressarcimento de ICMS retido por substituição tributária" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5605" descCfop:@"Transferência de saldo devedor de ICMS de outro estabelecimento da mesma empresa" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5650" descCfop:@"SAÍDAS DE COMBUSTÍVEIS, DERIVADOS OU NÃO DE PETRÓLEO E LUBRIFICANTES" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5651" descCfop:@"Venda de combustível ou lubrificante de produção do estabelecimento destinado à industrialização subseqüente" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5652" descCfop:@"Venda de combustível ou lubrificante de produção do estabelecimento destinado à comercialização" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5653" descCfop:@"Venda de combustível ou lubrificante de produção do estabelecimento destinado a consumidor ou usuário final" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5654" descCfop:@"Venda de combustível ou lubrificante adquirido ou recebido de terceiros destinado à industrialização subseqüente" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5655" descCfop:@"Venda de combustível ou lubrificante adquirido ou recebido de terceiros destinado à comercialização" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5656" descCfop:@"Venda de combustível ou lubrificante adquirido ou recebido de terceiros destinado a consumidor ou usuário final" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5657" descCfop:@"Remessa de combustível ou lubrificante adquirido ou recebido de terceiros para venda fora do estabelecimento" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5658" descCfop:@"Transferência de combustível ou lubrificante de produção do estabelecimento" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5659" descCfop:@"Transferência de combustível ou lubrificante adquirido ou recebido de terceiro" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5660" descCfop:@"Devolução de compra de combustível ou lubrificante adquirido para industrialização subseqüente" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5661" descCfop:@"Devolução de compra de combustível ou lubrificante adquirido para comercializaçã" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5662" descCfop:@"Devolução de compra de combustível ou lubrificante adquirido por consumidor ou usuário final" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5663" descCfop:@"Remessa para armazenagem de combustível ou lubrificante" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5664" descCfop:@"Retorno de combustível ou lubrificante recebido para armazenagem" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5665" descCfop:@"Retorno simbólico de combustível ou lubrificante recebido para armazenagem" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5666" descCfop:@"Remessa por conta e ordem de terceiros de combustível ou lubrificante recebido para armazenagem" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5901" descCfop:@"Remessa para industrialização por encomenda" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5902" descCfop:@"Retorno de mercadoria utilizada na industrialização por encomenda" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5903" descCfop:@"Retorno de mercadoria recebida para industrialização e não aplicada no referido processo" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5904" descCfop:@"Remessa para venda fora do estabelecimento" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5905" descCfop:@"Remessa para depósito fechado ou armazém geral" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5906" descCfop:@"Retorno de mercadoria depositada em depósito fechado ou armazém geral" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5907" descCfop:@"Retorno simbólico de mercadoria depositada em depósito fechado ou armazém geral" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5908" descCfop:@"Remessa de bem por conta de contrato de comodato" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5909" descCfop:@"Retorno de bem recebido por conta de contrato de comodato" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5910" descCfop:@"Remessa em bonificação, doação ou brinde" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5911" descCfop:@"Remessa de amostra grátis" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5912" descCfop:@"Remessa de mercadoria ou bem para demonstração" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5913" descCfop:@"Retorno de mercadoria ou bem recebido para demonstração" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5914" descCfop:@"Remessa de mercadoria ou bem para exposição ou feira" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5915" descCfop:@"Remessa de mercadoria ou bem para conserto ou reparo" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5916" descCfop:@"Retorno de mercadoria ou bem recebido para conserto ou reparo" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5917" descCfop:@"Remessa de mercadoria em consignação mercantil ou industrial" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5918" descCfop:@"Devolução de mercadoria recebida em consignação mercantil ou industrial" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5919" descCfop:@"Devolução simbólica de mercadoria vendida ou utilizada em processo industrial, recebida anteriormente em consignação mercantil ou industrial" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5920" descCfop:@"Remessa de vasilhame ou sacaria" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5921" descCfop:@"Devolução de vasilhame ou sacaria" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5922" descCfop:@"Lançamento efetuado a título de simples faturamento decorrente de venda para entrega futura" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5923" descCfop:@"Remessa de mercadoria por conta e ordem de terceiros, em venda à ordem" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5924" descCfop:@"Remessa para industrialização por conta e ordem do adquirente da mercadoria, " tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5925" descCfop:@"Retorno de mercadoria recebida para industrialização por conta e ordem do adquirente da mercadoria, quando aquela não transitar pelo estabelecimento do adquirente" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5926" descCfop:@"Lançamento efetuado a título de reclassificação de mercadoria decorrente de formação de kit ou de sua desagregação" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5927" descCfop:@"Lançamento efetuado a título de baixa de estoque decorrente de perda, roubo ou deterioração" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5928" descCfop:@"Lançamento efetuado a título de baixa de estoque decorrente do encerramento da atividade da empresa" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5929" descCfop:@"Lançamento efetuado em decorrência de emissão de documento fiscal relativo a operação ou prestação também registrada em equipamento Emissor de Cupom Fiscal" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5931" descCfop:@"Lançamento efetuado em decorrência da responsabilidade de retenção do imposto por substituição tributária, atribuída ao remetente ou alienante da mercadoria, pelo serviço de transporte realizado por transportador autônomo ou por transportador não inscrito na unidade da Federação onde iniciado o serviço" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5932" descCfop:@"Prestação de serviço de transporte iniciada em unidade da Federação diversa daquela onde inscrito o prestador" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5933" descCfop:@"Prestação de serviço tributado pelo ISSQN" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"5949" descCfop:@"Outra saída de mercadoria ou prestação de serviço não especificado" tipoCfop:@"Saida cliente do mesmo estado"],
                 [Cfop initWithCodCfop:@"6101" descCfop:@"Venda de produção do estabelecimento" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6102" descCfop:@"Venda de mercadoria adquirida ou recebida de terceiros" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6103" descCfop:@"Venda de produção do estabelecimento, efetuada fora do estabelecimento" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6104" descCfop:@"Venda de mercadoria adquirida ou recebida de terceiros, efetuada fora do estabelecimento" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6105" descCfop:@"Venda de produção do estabelecimento que não deva por ele transitar" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6106" descCfop:@"Venda de mercadoria adquirida ou recebida de terceiros, que não deva por ele transitar" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6107" descCfop:@"Venda de produção do estabelecimento, destinada a não contribuinte" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6108" descCfop:@"Venda de mercadoria adquirida ou recebida de terceiros, destinada a não contribuinte" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6109" descCfop:@"Venda de produção do estabelecimento, destinada à Zona Franca de Manaus ou Áreas de Livre Comércio" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6110" descCfop:@"Venda de mercadoria adquirida ou recebida de terceiros, destinada à Zona Franca de Manaus ou Áreas de Livre Comércio" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6111" descCfop:@"Venda de produção do estabelecimento remetida anteriormente em consignação industrial" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6112" descCfop:@"Venda de mercadoria adquirida ou recebida de Terceiros remetida anteriormente em consignação industrial" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6113" descCfop:@"Venda de produção do estabelecimento remetida anteriormente em consignação mercantil" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6114" descCfop:@"Venda de mercadoria adquirida ou recebida de terceiros remetida anteriormente em consignação mercantil" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6115" descCfop:@"Venda de mercadoria adquirida ou recebida de terceiros, recebida anteriormente em consignação mercantil" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6116" descCfop:@"Venda de produção do estabelecimento originada de encomenda para entrega futura" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6117" descCfop:@"Venda de mercadoria adquirida ou recebida de terceiros, originada de encomenda para entrega futura" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6118" descCfop:@"Venda de produção do estabelecimento entregue ao destinatário por conta e ordem do adquirente originário, em venda à ordem" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6119" descCfop:@"Venda de mercadoria adquirida ou recebida de terceiros entregue ao destinatário por conta e ordem do adquirente originário, em venda à ordem" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6120" descCfop:@"Venda de mercadoria adquirida ou recebida de terceiros entregue ao destinatário pelo vendedor remetente, em venda à ordem" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6122" descCfop:@"Venda de produção do estabelecimento remetida para industrialização, por conta e ordem do adquirente, sem transitar pelo estabelecimento do adquirente" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6123" descCfop:@"Venda de mercadoria adquirida ou recebida de terceiros remetida para industrialização, por conta e ordem do adquirente, sem transitar pelo estabelecimento do adquirente" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6124" descCfop:@"Industrialização efetuada para outra empresa" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6125" descCfop:@"Industrialização efetuada para outra empresa quando a mercadoria recebida para utilização no processo de industrialização não transitar pelo estabelecimento adquirente da mercadoria" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6151" descCfop:@"Transferência de produção do estabelecimento" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6152" descCfop:@"Transferência de mercadoria adquirida ou recebida de terceiros" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6153" descCfop:@"Transferência de energia elétrica" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6155" descCfop:@"Transferência de produção do estabelecimento, que não deva por ele transitar" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6156" descCfop:@"Transferência de mercadoria adquirida ou recebida de terceiros, que não deva por ele transitar" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6201" descCfop:@"Devolução de compra para industrialização" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6202" descCfop:@"Devolução de compra para comercialização" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6205" descCfop:@"Anulação de valor relativo a aquisição de serviço de comunicação" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6206" descCfop:@"Anulação de valor relativo a aquisição de serviço de transporte" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6207" descCfop:@"Anulação de valor relativo à compra de energia elétrica" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6208" descCfop:@"Devolução de mercadoria recebida em transferência para industrialização" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6209" descCfop:@"Devolução de mercadoria recebida em transferência para comercialização" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6210" descCfop:@"Devolução de compra para utilização na prestação de serviço" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6251" descCfop:@"Venda de energia elétrica para distribuição ou comercialização" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6252" descCfop:@"Venda de energia elétrica para estabelecimento industrial" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6253" descCfop:@"Venda de energia elétrica para estabelecimento comercial" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6254" descCfop:@"Venda de energia elétrica para estabelecimento prestador de serviço de transporte" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6255" descCfop:@"Venda de energia elétrica para estabelecimento prestador de serviço de comunicação" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6256" descCfop:@"Venda de energia elétrica para estabelecimento de produtor rural" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6257" descCfop:@"Venda de energia elétrica para consumo por demanda contratada" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6258" descCfop:@"Venda de energia elétrica a não contribuinte" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6301" descCfop:@"Prestação de serviço de comunicação para execução de serviço da mesma natureza" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6302" descCfop:@"Prestação de serviço de comunicação a estabelecimento industrial" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6303" descCfop:@"Prestação de serviço de comunicação a estabelecimento comercial" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6304" descCfop:@"Prestação de serviço de comunicação a estabelecimento de prestador de serviço de transporte" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6305" descCfop:@"Prestação de serviço de comunicação a estabelecimento de geradora ou de distribuidora de energia elétrica" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6306" descCfop:@"Prestação de serviço de comunicação a estabelecimento de produtor rural" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6307" descCfop:@"Prestação de serviço de comunicação a não contribuinte" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6351" descCfop:@"Prestação de serviço de transporte para execução de serviço da mesma natureza" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6352" descCfop:@"Prestação de serviço de transporte a estabelecimento industrial" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6353" descCfop:@"Prestação de serviço de transporte a estabelecimento comercial" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6354" descCfop:@"Prestação de serviço de transporte a estabelecimento de prestador de serviço de comunicação" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6355" descCfop:@"Prestação de serviço de transporte a estabelecimento de geradora ou de distribuidora de energia elétrica" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6356" descCfop:@"Prestação de serviço de transporte a estabelecimento de produtor rural" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6357" descCfop:@"Prestação de serviço de transporte a não contribuinte" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6359" descCfop:@"Prestação de serviço de transporte a contribuinte ou a não contribuinte quando a mercadoria transportada está dispensada de emissão de nota fiscal" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6401" descCfop:@"Venda de produção do estabelecimento em operação com produto sujeito ao regime de substituição tributária, na condição de contribuinte substituto" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6402" descCfop:@"Venda de produção do estabelecimento de produto sujeito ao regime de substituição tributária, em operação entre contribuintes substitutos do mesmo produto" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6403" descCfop:@"Venda de mercadoria adquirida ou recebida de terceiros em operação com mercadoria sujeita ao regime de substituição tributária, na condição de contribuinte substituto" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6404" descCfop:@"Venda de mercadoria sujeita ao regime de substituição tributária, cujo imposto já tenha sido retido anteriormente" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6408" descCfop:@"Transferência de produção do estabelecimento em operação com produto sujeito ao regime de substituição tributária" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6409" descCfop:@"Transferência de mercadoria adquirida ou recebida de terceiros em operação com mercadoria sujeita ao regime de substituição tributária" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6410" descCfop:@"Devolução de compra para industrialização em operação com mercadoria sujeita ao regime de substituição tributária" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6411" descCfop:@"Devolução de compra para comercialização em operação com mercadoria sujeita ao regime de substituição tributária" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6412" descCfop:@"Devolução de bem do ativo imobilizado, em operação com mercadoria sujeita ao regime de substituição tributária" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6413" descCfop:@"Devolução de mercadoria destinada ao uso ou consumo, em operação com mercadoria sujeita ao regime de substituição tributária" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6414" descCfop:@"Remessa de produção do estabelecimento para venda fora do estabelecimento em operação com produto sujeito ao regime de substituição tributária" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6415" descCfop:@"Remessa de mercadoria adquirida ou recebida de terceiros para venda fora do estabelecimento, em operação com mercadoria sujeita ao regime de substituição tributária" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6501" descCfop:@"Remessa de produção do estabelecimento, com fim específico de exportação" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6502" descCfop:@"Remessa de mercadoria adquirida ou recebida de terceiros, com fim específico de exportação" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6503" descCfop:@"Devolução de mercadoria recebida com fim específico de exportação" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6551" descCfop:@"Venda de bem do ativo imobilizado" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6552" descCfop:@"Transferência de bem do ativo imobilizado" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6553" descCfop:@"Devolução de compra de bem para o ativo imobilizado" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6554" descCfop:@"Remessa de bem do ativo imobilizado para uso fora do estabelecimento" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6555" descCfop:@"Devolução de bem do ativo imobilizado de terceiro, recebido para uso no estabelecimento" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6556" descCfop:@"Devolução de compra de material de uso ou consumo" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6557" descCfop:@"Transferência de material de uso ou consumo" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6603" descCfop:@"Ressarcimento de ICMS retido por substituição tributária" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6650" descCfop:@"SAÍDAS DE COMBUSTÍVEIS, DERIVADOS OU NÃO DE PETRÓLEO E LUBRIFICANTES" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6651" descCfop:@"Venda de combustível ou lubrificante de produção do estabelecimento destinado à industrialização subseqüente" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6652" descCfop:@"Venda de combustível ou lubrificante de produção do estabelecimento destinado à comercialização" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6653" descCfop:@"Venda de combustível ou lubrificante de produção do estabelecimento destinado a consumidor ou usuário final" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6654" descCfop:@"Venda de combustível ou lubrificante adquirido ou recebido de terceiros destinado à industrialização subseqüente" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6655" descCfop:@"Venda de combustível ou lubrificante adquirido ou recebido de terceiros destinado à comercialização" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6656" descCfop:@"Venda de combustível ou lubrificante adquirido ou recebido de terceiros destinado a consumidor ou usuário final" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6657" descCfop:@"Remessa de combustível ou lubrificante adquirido ou recebido de terceiros para venda fora do estabelecimento" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6658" descCfop:@"Transferência de combustível ou lubrificante de produção do estabelecimento" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6659" descCfop:@"Transferência de combustível ou lubrificante adquirido ou recebido de terceiro" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6660" descCfop:@"Devolução de compra de combustível ou lubrificante adquirido para industrialização subseqüente" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6661" descCfop:@"Devolução de compra de combustível ou lubrificante adquirido para comercialização" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6662" descCfop:@"Devolução de compra de combustível ou lubrificante adquirido por consumidor ou usuário final" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6663" descCfop:@"Remessa para armazenagem de combustível ou lubrificante" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6664" descCfop:@"Retorno de combustível ou lubrificante recebido para armazenagem" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6665" descCfop:@"Retorno simbólico de combustível ou lubrificante recebido para armazenagem" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6666" descCfop:@"Remessa por conta e ordem de terceiros de combustível ou lubrificante recebido para armazenagem" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6901" descCfop:@"Remessa para industrialização por encomenda" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6902" descCfop:@"Retorno de mercadoria utilizada na industrialização por encomenda" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6903" descCfop:@"Retorno de mercadoria recebida para industrialização e não aplicada no referido processo" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6904" descCfop:@"Remessa para venda fora do estabelecimento" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6905" descCfop:@"Remessa para depósito fechado ou armazém geral" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6906" descCfop:@"Retorno de mercadoria depositada em depósito fechado ou armazém geral" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6907" descCfop:@"Retorno simbólico de mercadoria depositada em depósito fechado ou armazém geral" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6908" descCfop:@"Remessa de bem por conta de contrato de comodato" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6909" descCfop:@"Retorno de bem recebido por conta de contrato de comodato" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6910" descCfop:@"Remessa em bonificação, doação ou brinde" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6911" descCfop:@"Remessa de amostra grátis" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6912" descCfop:@"Remessa de mercadoria ou bem para demonstração" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6913" descCfop:@"Retorno de mercadoria ou bem recebido para demonstração" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6914" descCfop:@"Remessa de mercadoria ou bem para exposição ou feira" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6915" descCfop:@"Remessa de mercadoria ou bem para conserto ou reparo" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6916" descCfop:@"Retorno de mercadoria ou bem recebido para conserto ou reparo" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6917" descCfop:@"Remessa de mercadoria em consignação mercantil ou industrial" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6918" descCfop:@"Devolução de mercadoria recebida em consignação mercantil ou industrial" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6919" descCfop:@"Devolução simbólica de mercadoria vendida ou utilizada em processo industrial, recebida anteriormente em consignação mercantil ou industrial" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6920" descCfop:@"Remessa de vasilhame ou sacaria" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6921" descCfop:@"Devolução de vasilhame ou sacaria" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6922" descCfop:@"Lançamento efetuado a título de simples faturamento decorrente de venda para entrega futura" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6923" descCfop:@"Remessa de mercadoria por conta e ordem de terceiros, em venda à ordem" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6924" descCfop:@"Remessa para industrialização por conta e ordem do adquirente da mercadoria, quando esta não transitar pelo estabelecimento do adquirente" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6925" descCfop:@"Retorno de mercadoria recebida para industrialização por conta e ordem do adquirente da mercadoria, quando aquela não transitar pelo estabelecimento do adquirente" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6929" descCfop:@"Lançamento efetuado em decorrência de emissão de documento fiscal relativo a operação ou prestação também registrada em equipamento Emissor de Cupom Fiscal" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6931" descCfop:@"Lançamento efetuado em decorrência da responsabilidade de retenção do imposto por substituição tributária, atribuída ao remetente ou alienante da mercadoria, pelo serviço de transporte realizado por transportador autônomo ou por transportador não inscrito na unidade da Federação onde iniciado o serviço" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6932" descCfop:@"Prestação de serviço de transporte iniciada em unidade da Federação diversa daquela onde inscrito o prestador" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6933" descCfop:@"Prestação de serviço tributado pelo ISSQN" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"6949" descCfop:@"Outra saída de mercadoria ou prestação de serviço não especificado" tipoCfop:@"Saida cliente outro estado"],
                 [Cfop initWithCodCfop:@"7101" descCfop:@"Venda de produção do estabelecimento" tipoCfop:@"Exterior"],
                 [Cfop initWithCodCfop:@"7102" descCfop:@"Venda de mercadoria adquirida ou recebida de terceiros" tipoCfop:@"Exterior"],
                 [Cfop initWithCodCfop:@"7105" descCfop:@"Venda de produção do estabelecimento, que não deva por ele transitar" tipoCfop:@"Exterior"],
                 [Cfop initWithCodCfop:@"7106" descCfop:@"Venda de mercadoria adquirida ou recebida de terceiros, que não deva por ele transitar" tipoCfop:@"Exterior"],
                 [Cfop initWithCodCfop:@"7127" descCfop:@"Venda de produção do estabelecimento sob o regime de “drawback”" tipoCfop:@"Exterior"],
                 [Cfop initWithCodCfop:@"7201" descCfop:@"Devolução de compra para industrialização" tipoCfop:@"Exterior"],
                 [Cfop initWithCodCfop:@"7202" descCfop:@"Devolução de compra para comercialização" tipoCfop:@"Exterior"],
                 [Cfop initWithCodCfop:@"7205" descCfop:@"Anulação de valor relativo à aquisição de serviço de comunicação" tipoCfop:@"Exterior"],
                 [Cfop initWithCodCfop:@"7206" descCfop:@"Anulação de valor relativo a aquisição de serviço de transporte" tipoCfop:@"Exterior"],
                 [Cfop initWithCodCfop:@"7207" descCfop:@"Anulação de valor relativo à compra de energia elétrica" tipoCfop:@"Exterior"],
                 [Cfop initWithCodCfop:@"7210" descCfop:@"Devolução de compra para utilização na prestação de serviço" tipoCfop:@"Exterior"],
                 [Cfop initWithCodCfop:@"7211" descCfop:@"Devolução de compras para industrialização sob o regime de drawback”" tipoCfop:@"Exterior"],
                 [Cfop initWithCodCfop:@"7251" descCfop:@"Venda de energia elétrica para o exterior" tipoCfop:@"Exterior"],
                 [Cfop initWithCodCfop:@"7301" descCfop:@"Prestação de serviço de comunicação para execução de serviço da mesma natureza" tipoCfop:@"Exterior"],
                 [Cfop initWithCodCfop:@"7358" descCfop:@"Prestação de serviço de transporte" tipoCfop:@"Exterior"],
                 [Cfop initWithCodCfop:@"7501" descCfop:@"Exportação de mercadorias recebidas com fim específico de exportação" tipoCfop:@"Exterior"],
                 [Cfop initWithCodCfop:@"7551" descCfop:@"Venda de bem do ativo imobilizado" tipoCfop:@"Exterior"],
                 [Cfop initWithCodCfop:@"7553" descCfop:@"Devolução de compra de bem para o ativo imobilizado" tipoCfop:@"Exterior"],
                 [Cfop initWithCodCfop:@"7556" descCfop:@"Devolução de compra de material de uso ou consumo" tipoCfop:@"Exterior"],
                 [Cfop initWithCodCfop:@"7650" descCfop:@"SAÍDAS DE COMBUSTÍVEIS, DERIVADOS OU NÃO DE PETRÓLEO E LUBRIFICANTES" tipoCfop:@"Exterior"],
                 [Cfop initWithCodCfop:@"7651" descCfop:@"Venda de combustível ou lubrificante de produção do estabelecimento" tipoCfop:@"Exterior"],
                 [Cfop initWithCodCfop:@"7654" descCfop:@"Venda de combustível ou lubrificante adquirido ou recebido de terceiros" tipoCfop:@"Exterior"],
                 [Cfop initWithCodCfop:@"7930" descCfop:@"Lançamento efetuado a título de devolução de bem cuja entrada tenha ocorrido sob amparo de regime especial aduaneiro de admissão temporária" tipoCfop:@"Exterior"],
                 [Cfop initWithCodCfop:@"7949" descCfop:@"Outra saída de mercadoria ou prestação de serviço não especificado" tipoCfop:@"Exterior"],nil];
    self.filteredCfopArray = [NSMutableArray arrayWithCapacity:[cfopArray count]];
    [self.tableView reloadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"CfopDetailSegue"]){
        //   UIViewController *candyDetailViewController = [segue destinationViewController];
        // In order to manipulate the destination view controller, another check on which table (search or normal) is displayed is needed
        if(sender == self.searchDisplayController.searchResultsTableView) {
            
           // UITableViewCell *cell = (UITableViewCell *)sender;
          
            NSIndexPath *ip=[self.searchDisplayController.searchResultsTableView
                             indexPathForSelectedRow];
            
            Cfop *p=[self.filteredCfopArray objectAtIndex:ip.row];
            
            CfopDetailTableViewController *pdvc= (CfopDetailTableViewController *)segue.destinationViewController;
            pdvc.cfop=p;
        }
        else {
            
            UITableViewCell *cell = (UITableViewCell *)sender;
            NSIndexPath *ip=[self.tableView indexPathForCell:cell];
            Cfop *p=[self.cfopArray objectAtIndex:ip.row];
            
            CfopDetailTableViewController *pdvc= (CfopDetailTableViewController *)segue.destinationViewController;
            pdvc.cfop=p;
            
        }
    }
}
#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [filteredCfopArray count];
        
    } else {
        return [cfopArray count];
        
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if ( cell == nil ) {
        //      cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    // Create a new Candy Object
    Cfop *cfop = nil;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        cfop = [filteredCfopArray objectAtIndex:indexPath.row];
    } else {
        cfop = [cfopArray objectAtIndex:indexPath.row];
    }    // Configure the cell
    cell.textLabel.text = cfop.codCfop;
    cell.detailTextLabel.text=cfop.descCfop;
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    return cell;
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        [self performSegueWithIdentifier:@"CfopDetailSegue" sender:tableView];
    }
}

@end
