//
//  Cst.m
//  Icfop
//
//  Created by Felix Canaparro on 20/04/13.
//  Copyright (c) 2013 Felix Canaparro. All rights reserved.
//

#import "Cst.h"

@implementation Cst

@synthesize descCst,codCst,tipoCst;
+(id)initWithCodCst:(NSString *)aCodCst descCst:(NSString *)aDescCst tipoCst:(NSString *)aTipoCst
{
    
    
    Cst *newCst =[[self alloc]init];
    newCst.codCst=aCodCst;
    newCst.descCst=aDescCst;
    newCst.tipoCst=aTipoCst;
    return newCst;
}
@end
