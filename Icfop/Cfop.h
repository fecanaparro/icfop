//
//  Cfop.h
//  Icfop
//
//  Created by Felix Canaparro on 19/03/13.
//  Copyright (c) 2013 Felix Canaparro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Cfop : NSObject{
    NSString *codCfop;
    NSString *descCfop;
    NSString *tipoCfop;
    
}
@property(nonatomic,copy)NSString *codCfop;
@property(nonatomic,copy)NSString *descCfop;
@property(nonatomic,copy)NSString *tipoCfop;
+(id)initWithCodCfop:(NSString *)aCodCfop descCfop:(NSString *)aDescCfop tipoCfop:(NSString *)aTipoCfop;
@end
