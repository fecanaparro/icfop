//
//  Cfop.m
//  Icfop
//
//  Created by Felix Canaparro on 19/03/13.
//  Copyright (c) 2013 Felix Canaparro. All rights reserved.
//

#import "Cfop.h"

@implementation Cfop
@synthesize descCfop,codCfop,tipoCfop;
+(id)initWithCodCfop:(NSString *)aCodCfop descCfop:(NSString *)aDescCfop tipoCfop:(NSString *)aTipoCfop
{
    
    
    Cfop *newCfop =[[self alloc]init];
    newCfop.codCfop=aCodCfop;
    newCfop.descCfop=aDescCfop;
    newCfop.tipoCfop=aTipoCfop;
    return newCfop;
}
@end
