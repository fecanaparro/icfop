//
//  Cst.h
//  Icfop
//
//  Created by Felix Canaparro on 20/04/13.
//  Copyright (c) 2013 Felix Canaparro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface  Cst : NSObject{
    NSString *codCst;
    NSString *descCst;
    NSString *tipoCst;
    
}
@property(nonatomic,copy)NSString *codCst;
@property(nonatomic,copy)NSString *descCst;
@property(nonatomic,copy)NSString *tipoCst;
+(id)initWithCodCst:(NSString *)aCodCst descCst:(NSString *)aDescCst tipoCst:(NSString *)aTipoCst;

@end
