//
//  CSTICMSListViewController.m
//  Icfop
//
//  Created by Felix Canaparro on 20/04/13.
//  Copyright (c) 2013 Felix Canaparro. All rights reserved.
//

#import "CSTICMSListViewController.h"
#import "Cst.h"
#import "CSTICMSDetailTableViewController.h"
@interface CSTICMSListViewController ()

@end

@implementation CSTICMSListViewController
@synthesize cstICMSArray;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
  
    cstICMSArray= [NSArray arrayWithObjects:
                  [Cst initWithCodCst:@"00" descCst:@"Tributada integralmente"	 tipoCst:@"ICMS"],
                  [Cst initWithCodCst:@"10" descCst:@"Tributada e com cobrança do ICMS por substituição tributária"	 tipoCst:@"ICMS"],
                  [Cst initWithCodCst:@"20" descCst:@"Com redução de base de cálculo" tipoCst:@"ICMS"],
                  [Cst initWithCodCst:@"30" descCst:@"Isenta ou não tributada e com cobrança do ICMS por substituição tributária"	 tipoCst:@"ICMS"],
                  [Cst initWithCodCst:@"40" descCst:@"Isenta"tipoCst:@"ICMS"],
                  [Cst initWithCodCst:@"41" descCst:@"Não Tributada"	 tipoCst:@"ICMS"],
                  [Cst initWithCodCst:@"50" descCst:@"Suspensão" tipoCst:@"ICMS"],
                  [Cst initWithCodCst:@"51" descCst:@"Diferimento" tipoCst:@"ICMS"],
                   [Cst initWithCodCst:@"60" descCst:@"ICMS cobrado anteriormente por substituição tributária"	 tipoCst:@"ICMS"],
                  [Cst initWithCodCst:@"70" descCst:@"Com redução de base de cálculo e cobrança do ICMS por substituição tributária"	 tipoCst:@"ICMS"],
                  [Cst initWithCodCst:@"90" descCst:@"Outras"	 tipoCst:@"ICMS"]
                  ,nil];
   
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"ICMSDetailSegue"]){
        UITableViewCell *cell = (UITableViewCell *)sender;
        NSIndexPath *ip=[self.tableView indexPathForCell:cell];
        Cst *p=[self.cstICMSArray objectAtIndex:ip.row];
        
        CSTICMSDetailTableViewController *pdvc= (CSTICMSDetailTableViewController *)segue.destinationViewController;
        pdvc.cst=p;
        
    }
    
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [cstICMSArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if ( cell == nil ) {
        //      cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    // Create a new Candy Object
    Cst *cst = nil;
    cst = [cstICMSArray  objectAtIndex:indexPath.row];
    cell.textLabel.text = cst.codCst;
    cell.detailTextLabel.text=cst.descCst;
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    return cell;
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   // [self performSegueWithIdentifier:@"ICMSDetailSegue" sender:tableView];
     
}

@end
