//
//  CSTPISCOFDetailTableViewController.m
//  Icfop
//
//  Created by Felix Canaparro on 21/04/13.
//  Copyright (c) 2013 Felix Canaparro. All rights reserved.
//

#import "CSTPISCOFDetailTableViewController.h"
#import "Cst.h"
@interface CSTPISCOFDetailTableViewController ()

@end

@implementation CSTPISCOFDetailTableViewController

@synthesize  codCstLabel,descCstLabel,tipoCstLabel,cst;





- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.descCstLabel.text=self.cst.descCst;
    self.codCstLabel.text=self.cst.codCst;
    self.tipoCstLabel.text=self.cst.tipoCst;
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source





@end









