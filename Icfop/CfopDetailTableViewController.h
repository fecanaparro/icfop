//
//  CfopDetailTableViewController.h
//  Icfop
//
//  Created by Felix Canaparro on 19/03/13.
//  Copyright (c) 2013 Felix Canaparro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Cfop.h"
@interface CfopDetailTableViewController : UITableViewController

@property (weak) IBOutlet UILabel *codCfopLabel;
@property (weak) IBOutlet UILabel *descCfopLabel;
@property (weak) IBOutlet UILabel *tipofopLabel;
@property(strong) Cfop *cfop;
@end
