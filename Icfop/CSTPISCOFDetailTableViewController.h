//
//  CSTPISCOFDetailTableViewController.h
//  Icfop
//
//  Created by Felix Canaparro on 21/04/13.
//  Copyright (c) 2013 Felix Canaparro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Cst.h"
@interface CSTPISCOFDetailTableViewController : UITableViewController
@property (weak) IBOutlet UILabel *codCstLabel;
@property (weak) IBOutlet UILabel *descCstLabel;
@property (weak) IBOutlet UILabel *tipoCstLabel;
@property(strong) Cst *cst;
@end


