//
//  CSTPISCOFViewController.h
//  Icfop
//
//  Created by Felix Canaparro on 20/04/13.
//  Copyright (c) 2013 Felix Canaparro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Cst.h"
#import "CSTPISCOFDetailTableViewController.h"
@interface CSTPISCOFViewController : UITableViewController <UITableViewDataSource>
@property (strong,nonatomic) NSArray *cstPISCOFArray;

@end
