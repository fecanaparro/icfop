//
//  CfopDetailTableViewController.m
//  Icfop
//
//  Created by Felix Canaparro on 19/03/13.
//  Copyright (c) 2013 Felix Canaparro. All rights reserved.
//

#import "CfopDetailTableViewController.h"

@interface CfopDetailTableViewController ()

@end

@implementation CfopDetailTableViewController

@synthesize  codCfopLabel,descCfopLabel,tipofopLabel,cfop;





- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.descCfopLabel.text=self.cfop.descCfop;
    self.codCfopLabel.text=self.cfop.codCfop;
    self.tipofopLabel.text=self.cfop.tipoCfop;
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source





@end






































































































