//
//  Icfop_AppDelegate.h
//  Icfop
//
//  Created by Felix Canaparro on 19/03/13.
//  Copyright (c) 2013 Felix Canaparro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Icfop_AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
