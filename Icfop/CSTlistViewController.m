//
//  CSTlistViewController.m
//  Icfop
//
//  Created by Felix Canaparro on 20/04/13.
//  Copyright (c) 2013 Felix Canaparro. All rights reserved.
//

#import "CSTlistViewController.h"
#import "Cst.h"
@interface CSTlistViewController ()


@end

@implementation CSTlistViewController
@synthesize cstIPIArray;
@synthesize cstPISCOFArray;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
  
    cstIPIArray= [NSArray arrayWithObjects:
            [Cst initWithCodCst:@"00" descCst:@"Entrada com Recuperação de Crédito"	 tipoCst:@"IPI"],
            [Cst initWithCodCst:@"01" descCst:@"Entrada Tributável com Alíquota Zero"	 tipoCst:@"IPI"],
            [Cst initWithCodCst:@"02" descCst:@"Entrada Isenta" tipoCst:@"IPI"],
            [Cst initWithCodCst:@"03" descCst:@"Entrada Não Tributada"	 tipoCst:@"IPI"],
            [Cst initWithCodCst:@"04" descCst:@"Entrada Imune"tipoCst:@"IPI"],
            [Cst initWithCodCst:@"05" descCst:@"Entrada com Suspensão"	 tipoCst:@"IPI"],
            [Cst initWithCodCst:@"49" descCst:@"Outras Entradas" tipoCst:@"IPI"],
            [Cst initWithCodCst:@"50" descCst:@"Saída Tributada" tipoCst:@"IPI"],
            [Cst initWithCodCst:@"51" descCst:@"Saída Tributável com Alíquota Zero"	 tipoCst:@"IPI"],
            [Cst initWithCodCst:@"52" descCst:@"Saída Isenta"	 tipoCst:@"IPI"],
            [Cst initWithCodCst:@"53" descCst:@"Saída Não Tributada"	 tipoCst:@"IPI"],
            [Cst initWithCodCst:@"54" descCst:@"Saída Imune"	 tipoCst:@"IPI"],
            [Cst initWithCodCst:@"55" descCst:@"Saída com Suspensão"	 tipoCst:@"IPI"],
            [Cst initWithCodCst:@"99" descCst:@"Outras Saídas"	 tipoCst:@"IPI"]
                  ,nil];
    
    [self.tableView reloadData];


    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [cstIPIArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIPI";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if ( cell == nil ) {
        //      cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    // Create a new Candy Object
    Cst *cst = nil;
        cst = [cstIPIArray  objectAtIndex:indexPath.row];
    cell.textLabel.text = cst.codCst;
    cell.detailTextLabel.text=cst.descCst;
    return cell;

}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
