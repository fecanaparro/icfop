//
//  CSTlistViewController.h
//  Icfop
//
//  Created by Felix Canaparro on 20/04/13.
//  Copyright (c) 2013 Felix Canaparro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CSTlistViewController : UITableViewController
@property (strong,nonatomic) NSArray *cstIPIArray;
@property (strong,nonatomic) NSArray *cstPISCOFArray;
@end
