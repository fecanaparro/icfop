//
//  CSTPISCOFViewController.m
//  Icfop
//
//  Created by Felix Canaparro on 20/04/13.
//  Copyright (c) 2013 Felix Canaparro. All rights reserved.
//

#import "CSTPISCOFViewController.h"
#import "Cst.h"
#import "CSTPISCOFDetailTableViewController.h"
@interface CSTPISCOFViewController ()

@end

@implementation CSTPISCOFViewController
@synthesize cstPISCOFArray;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    cstPISCOFArray= [NSArray arrayWithObjects:
    [Cst initWithCodCst:@"01" descCst:@"Operação Tributável com Alíquota Básica" tipoCst:@"SAIDA PISCOF"],
    [Cst initWithCodCst:@"02" descCst:@"Operação Tributável com Alíquota Diferenciada" tipoCst:@"SAIDA PISCOF"],
    [Cst initWithCodCst:@"03" descCst:@"Operação Tributável com Alíquota por Unidade de Medida de Produto	" tipoCst:@"SAIDA PISCOF"],
    [Cst initWithCodCst:@"04" descCst:@"Operação Tributável Monofásica - Revenda a Alíquota Zero	" tipoCst:@"SAIDA PISCOF"],
    [Cst initWithCodCst:@"05" descCst:@"Operação Tributável por Substituição Tributária" tipoCst:@"SAIDA PISCOF"],
    [Cst initWithCodCst:@"06" descCst:@"Operação Tributável a Alíquota Zero	" tipoCst:@"SAIDA PISCOF"],
    [Cst initWithCodCst:@"07" descCst:@"Operação Isenta da Contribuição	" tipoCst:@"SAIDA PISCOF"],
    [Cst initWithCodCst:@"08" descCst:@"Operação sem Incidência da Contribuição	" tipoCst:@"SAIDA PISCOF"],
    [Cst initWithCodCst:@"09" descCst:@"Operação com Suspensão da Contribuição	" tipoCst:@"SAIDA PISCOF"],
    [Cst initWithCodCst:@"49" descCst:@"Outras Operações de Saída	" tipoCst:@"SAIDA PISCOF"],
    [Cst initWithCodCst:@"50" descCst:@"Operação com Direito a Crédito - Vinculada Exclusivamente a Receita Tributada no Mercado Interno" tipoCst:@"ENTRADA PISCOF"],
    [Cst initWithCodCst:@"51" descCst:@"Operação com Direito a Crédito – Vinculada Exclusivamente a Receita Não Tributada no Mercado Interno" tipoCst:@"ENTRADA PISCOF"],
    [Cst initWithCodCst:@"52" descCst:@"Operação com Direito a Crédito - Vinculada Exclusivamente a Receita de Exportação	" tipoCst:@"ENTRADA PISCOF"],
    [Cst initWithCodCst:@"53" descCst:@"Operação com Direito a Crédito - Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno	" tipoCst:@"ENTRADA PISCOF"],
    [Cst initWithCodCst:@"54" descCst:@"Operação com Direito a Crédito - Vinculada a Receitas Tributadas no Mercado Interno e de Exportação	" tipoCst:@"ENTRADA PISCOF"],
    [Cst initWithCodCst:@"55" descCst:@"Operação com Direito a Crédito - Vinculada a Receitas Não-Tributadas no Mercado Interno e de Exportação	" tipoCst:@"ENTRADA PISCOF"],
    [Cst initWithCodCst:@"56" descCst:@"Operação com Direito a Crédito - Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno, e de Exportação	" tipoCst:@"ENTRADA PISCOF"],
    [Cst initWithCodCst:@"60" descCst:@"Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita Tributada no Mercado Interno	" tipoCst:@"ENTRADA PISCOF"],
    [Cst initWithCodCst:@"61" descCst:@"Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita Não-Tributada no Mercado Interno	" tipoCst:@"ENTRADA PISCOF"],
    [Cst initWithCodCst:@"62" descCst:@"Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita de Exportação" tipoCst:@"ENTRADA PISCOF"],
    [Cst initWithCodCst:@"63" descCst:@"Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno	" tipoCst:@"ENTRADA PISCOF"],
    [Cst initWithCodCst:@"64" descCst:@"Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas no Mercado Interno e de Exportação	" tipoCst:@"ENTRADA PISCOF"],
    [Cst initWithCodCst:@"65" descCst:@"Crédito Presumido - Operação de Aquisição Vinculada a Receitas Não-Tributadas no Mercado Interno e de Exportação	" tipoCst:@"ENTRADA PISCOF"],
    [Cst initWithCodCst:@"66" descCst:@"Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno, e de Exportação	" tipoCst:@"ENTRADA PISCOF"],
     [Cst initWithCodCst:@"67" descCst:@"Crédito Presumido - Outras Operações" tipoCst:@"ENTRADA PISCOF"],
     [Cst initWithCodCst:@"70" descCst:@"Operação de Aquisição sem Direito a Crédito	" tipoCst:@"ENTRADA PISCOF"],
     [Cst initWithCodCst:@"71" descCst:@"Operação de Aquisição com Isenção" tipoCst:@"ENTRADA PISCOF"],
     [Cst initWithCodCst:@"72" descCst:@"Operação de Aquisição com Suspensão" tipoCst:@"ENTRADA PISCOF"],
     [Cst initWithCodCst:@"73" descCst:@"Operação de Aquisição a Alíquota Zero" tipoCst:@"ENTRADA PISCOF"],
     [Cst initWithCodCst:@"74" descCst:@"Operação de Aquisição sem Incidência da Contribuição" tipoCst:@"ENTRADA PISCOF"],
     [Cst initWithCodCst:@"75" descCst:@"Operação de Aquisição por Substituição Tributária" tipoCst:@"ENTRADA PISCOF"],
     [Cst initWithCodCst:@"98" descCst:@"Outras Operações de Entrada" tipoCst:@"ENTRADA PISCOF"],
     [Cst initWithCodCst:@"99" descCst:@"Outras Operações" tipoCst:@"ENTRADA PISCOF"],
     nil];
//    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [cstPISCOFArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if ( cell == nil ) {
        //      cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    // Create a new Candy Object
    Cst *cst = nil;
    cst = [cstPISCOFArray  objectAtIndex:indexPath.row];
    cell.textLabel.text = cst.codCst;
    cell.detailTextLabel.text=cst.descCst;
    return cell;
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"PISCOFDetailSegue"]){
        UITableViewCell *cell = (UITableViewCell *)sender;
        NSIndexPath *ip=[self.tableView indexPathForCell:cell];
        Cst *p=[self.cstPISCOFArray objectAtIndex:ip.row];
        
        CSTPISCOFDetailTableViewController *pdvc= (CSTPISCOFDetailTableViewController *)segue.destinationViewController;
        pdvc.cst=p;
        
    }
}
/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
 //[self performSegueWithIdentifier:@"PISCOFDetailSegue" sender:tableView];
}

@end
