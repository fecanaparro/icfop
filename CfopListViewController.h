//
//  CfopListViewController.h
//  Icfop
//
//  Created by Felix Canaparro on 19/03/13.
//  Copyright (c) 2013 Felix Canaparro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CfopListViewController : UITableViewController <UISearchBarDelegate, UISearchDisplayDelegate, UITableViewDataSource>
@property (strong,nonatomic) NSArray *cfopArray;
@property (nonatomic,retain) NSArray *filteredCfopArray;
@property IBOutlet UISearchBar *cfopSearchBar;

@end
